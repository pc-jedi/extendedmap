"use strict";

module.exports = {
    require: ['esm', 'ts-node/register'],
    'full-trace': true,
    recursive: true,
    exit: true
}
