
export class ExtendedMap<K, V> extends Map<K, V> {
    public get length(): number {
        return this.size;
    }

    public getOrElseWith(key: K, orElse: (key: K) => V): V {
        const res: V | undefined = this.get(key);

        if (res === undefined) {
            return orElse(key);
        }

        return res;
    }

    public getOrElse(key: K, orElse: V): V {
        const res: V | undefined = this.get(key);

        if (res === undefined) {
            return orElse;
        }

        return res;
    }

    public getOrCreate(key: K, orCreate: (key: K) => V): V {
        let res: V | undefined = this.get(key);

        if (res === undefined) {
            res = orCreate(key);
            this.set(key, res);
        }

        return res;
    }

    public getOrFail(key: K): V {
        const res: V | undefined = this.get(key);
        if (res === undefined) {
            throw new Error(`key '${String(key)}' is not part of the map-keys [${this.keysArray().join(', ')}]`);
        }

        return res;
    }

    public map<M>(mapper: (key: K, value: V) => M): ExtendedMap<K, M> {
        const keys = this.keys();
        const res: ExtendedMap<K, M> = new ExtendedMap<K, M>();

        for (const k of keys) {
            res.set(k, mapper(k, this.getOrFail(k)));
        }

        return res;
    }

    public mapToArray<M>(mapper: (key: K, value: V) => M): M[] {
        const keys = this.keys();
        const res: M[] = [];

        for (const k of keys) {
            res.push(mapper(k, this.getOrFail(k)));
        }

        return res;
    }

    public filter(filter: (key: K, value: V) => boolean): ExtendedMap<K, V> {
        const keys = this.keys();
        const res: ExtendedMap<K, V> = new ExtendedMap<K, V>();

        for (const key of keys) {
            const value = this.getOrFail(key);

            if (filter(key, value)) {
                res.set(key, value);
            }
        }

        return res;
    }

    public reduce<U>(reducer: (previousValue: U, currentKey: K, currentValue: V, map: ExtendedMap<K, V>) => U,
                     initialValue: U): U {
        let res = initialValue;

        this.forEach((value: V, key: K) => {
            res = reducer(res, key, value, this);
        });

        return res;
    }

    public keysArray(): K[] {
        return [...this.keys()];
    }

    public valuesArray(): V[] {
        return [...this.values()];
    }

    public toObject(): Record<string, V> {
        const res = Object.create(null) as Record<string, V>;

        for (const [key, value] of this) {
            const stringKey = String(key);
            res[stringKey] = value;
        }

        return res;
    }

    public toTuple(): [K, V][] {
        return this.mapToArray((key: K, value: V) => [key, value]);
    }

    public getAndSet(key: K, setter: (value: V) => V): this {
        const value = this.getOrFail(key);
        this.set(key, setter(value));

        return this;
    }
}
