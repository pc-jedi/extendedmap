/**
 *
 */

import {assert} from 'chai';
import {ExtendedMap} from '../src';

describe('ExtendedMap', () => {
    describe('#map', () => {
        const extendedMap = new ExtendedMap<string, number>([['a', 1], ['b', 2], ['c', 3]]);

        it('should call mapper for each entry', () => {
            const actual = extendedMap.map((key, value) => {
                return `${key}-${value}`;
            });
            const expected = new ExtendedMap<string, string>([['a', 'a-1'], ['b', 'b-2'], ['c', 'c-3']]);

            assert.deepEqual(actual, expected);
        });
    });
});
